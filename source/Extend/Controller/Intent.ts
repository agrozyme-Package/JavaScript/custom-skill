import ResolutionStatusCode from '@agrozyme/alexa-skill-type/source/Custom/Enumeration/ResolutionStatusCode';
import {IntentRequest, SlotValue} from 'alexa-sdk';
import Base from '../Base/Controller';
import Skill from '../Skill';

export type IntentConstructor = { new(skill: Skill): Intent };

abstract class Intent extends Base {

  // noinspection JSUnusedGlobalSymbols
  protected getSlot(index: string): SlotValue {
    const request: IntentRequest = this.skill.event.request;
    const items: Record<string, SlotValue> = (undefined === request.intent) ? {} : request.intent.slots;
    return (<object>items).hasOwnProperty(index) ? items[index] : {name: ''};
  }

  // noinspection JSUnusedGlobalSymbols
  protected setupDialogDelegate(): number {
    const request: IntentRequest = this.skill.event.request;
    const response = this.skill.response.response;
    response.shouldEndSession = false;

    if (undefined === response.directives) {
      response.directives = [];
    }

    const directives: object[] = response.directives;
    directives.push({
      type: 'Dialog.Delegate',
      updatedIntent: this.clone(request.intent)
    });
    return (directives.length - 1);
  }

  // noinspection FunctionWithMultipleReturnPointsJS
  protected updateDialogDelegate(name: string) {
    const intent = (<IntentRequest>this.skill.event.request).intent;

    if ((undefined === intent) || (false === (<object>intent.slots).hasOwnProperty(name))) {
      return;
    }

    const index = this.setupDialogDelegate();
    const directive = this.skill.response.response.directives[index];
    const slot: SlotValue = directive.updatedIntent.slots[name];
    delete slot.value;
    delete slot.resolutions;
  }

  // noinspection FunctionWithMultipleReturnPointsJS
  protected matchSynonyms(name: string): { name: string; synonym: string; values: Set<string>; } {
    const slot = this.getSlot(name);
    const data = {
      name,
      synonym: slot.value,
      values: new Set<string>()
    };
    const resolutions = slot.resolutions;

    if (undefined === resolutions) {
      return data;
    }

    const authority = resolutions.resolutionsPerAuthority;

    if (undefined === authority) {
      return data;
    }

    authority
      .filter(item => (ResolutionStatusCode.ER_SUCCESS_MATCH === item.status.code))
      .forEach(item => item.values.forEach(item => data.values.add(item.value.name)));

    return data;
  }

  protected getSlotValue(name: string, errorText: string = `Not fill slot: ${name}`) {
    const item = this.getSlot(name).value;

    if (undefined === item) {
      this.updateDialogDelegate(name);
      throw new Error(errorText);
    }

    return item;
  }

  protected getSlotSynonym(name: string, errorText: string = `Not fill slot: ${name}`): { name: string; synonym: string; value: string; } {
    const {synonym, values} = this.matchSynonyms(name);

    if (0 === values.size) {
      this.updateDialogDelegate(name);
      throw new Error(errorText);
    }

    return {
      name,
      synonym,
      value: values.values().next().value
    };
  }

}

export default Intent;
