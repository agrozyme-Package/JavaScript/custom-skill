import Base from '../Base/Controller';

class Launch extends Base {
  async run(): Promise<any> {
    this.skill.getResponse().shouldEndSession = false;
  }

}

export default Launch;
